let values = [];

let i = 0;

function setup() {
    createCanvas(windowWidth, windowHeight);
    fillValues();
}

function draw() {
    background(255);

    if(i < values.length) {
        for (let index = 0; index < 100; index++) {
            if(i <= values.length) {
                for (let j = 0; j < values.length - i - 1; j++) {
                    if(values[j] > values[j + 1]) {
                        swap(values, j, j + 1);
                    }
                }
                i++;
            }
        }
    } else {
        console.log("finished");
        noLoop();
        /* fillValues();
        i = 0; */
    }

    for (let n = 0; n < values.length; n++) {
        stroke(255, 0, 0);
        line(n, height, n, values[n]);
    }
}

function fillValues() {
    values = [];
    for (let i = 0; i < width; i++){
		// noise is more beautiful
        values.push(noise(i / 100.0)* height);
		// random values
        // values.push(random(height));
    }
}

function swap(arr, a, b) {
    let temp = arr[a];
    arr[a] = arr[b];
    arr[b] = temp;
}